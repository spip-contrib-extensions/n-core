<?php
/**
 * Gestion des autorisations du plugin N-Core liées à l'utilisation des API.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction appelée par le pipeline.
 *
 * @pipeline autoriser
 */
function ncore_autoriser() {
}

/**
 * Autorisation d'édition d'une noisette déjà ajoutée dans un conteneur.
 * Il faut :
 * - que la noisette existe bien.
 *
 * @package SPIP\NCORE\NOISETTE\AUTORISATION
 *
 * @uses noisette_lire()
 *
 * @param string         $faire   L'action :  l'édition (editer)
 * @param string         $type    Le type d'objet ou nom de table : une noisette
 * @param int            $id      Id de l'objet sur lequel on veut agir : identifiant de la noisette
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `plugin` et `stockage`
 *
 * @return bool `true` si autorisé, `false` sinon.
 */
function autoriser_noisette_editer_dist($faire, $type, $id, $qui, $options) {
	$autoriser = false;

	if (
		include_spip('ncore/ncore')
		and !empty($options['plugin'])
		and !empty($options['stockage'])
		and ($id_noisette = ncore_noisette_verifier_identifiant($options['plugin'], $id, $options['stockage']))
		and include_spip('inc/ncore_noisette')
		and noisette_lire('noizetier', $id_noisette, 'id_conteneur')
	) {
		$autoriser = true;
	}

	return $autoriser;
}
