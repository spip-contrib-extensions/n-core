<?php
/**
 * Ce fichier contient l'API N-Core de gestion des noisettes, c'est-à-dire les instances paramétrées
 * de types de noisette affectées à un conteneur.
 *
 * @package SPIP\NCORE\NOISETTE\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajoute dans un conteneur, à un rang donné ou en dernier rang, une noisette d'un type donné.
 * La fonction met à jour les rangs des autres noisettes si nécessaire.
 *
 * @api
 *
 * @uses type_noisette_lire()
 * @uses ncore_conteneur_verifier()
 * @uses ncore_conteneur_identifier()
 * @uses ncore_conteneur_construire()
 * @uses ncore_conteneur_est_noisette()
 * @uses ncore_noisette_decrire()
 * @uses ncore_noisette_completer_description()
 * @uses ncore_noisette_lister()
 * @uses ncore_noisette_ranger()
 * @uses ncore_noisette_stocker()
 * @uses ncore_noisette_completer_action()
 *
 * @param string       $plugin        Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier ou
 *                                    un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param string       $type_noisette Identifiant du type de noisette à ajouter au squelette.
 * @param array|string $conteneur     Identifiant du conteneur accueillant la noisette qui prend soit la forme d'un tableau soit celui d'un id.
 * @param null|int     $rang          Rang dans le conteneur où insérer la noisette. Si l'argument n'est pas fourni ou est égal à 0
 *                                    on insère la noisette en fin de conteneur.
 * @param null|string  $stockage      Identifiant du service de stockage à utiliser si précisé. Dans ce cas, ni celui du plugin
 *                                    ni celui de N-Core ne seront utilisés. En général, cet identifiant est le préfixe d'un plugin
 *                                    fournissant le service de stockage souhaité.
 *
 * @return bool|int|string Retourne l'identifiant de la nouvelle instance de noisette créée ou `false` en cas d'erreur.
 **/
function noisette_ajouter(string $plugin, string $type_noisette, $conteneur, ?int $rang = 0, ?string $stockage = '') {
	// Initialisation de la valeur de sortie.
	$noisette_ajoutee = false;

	if ($type_noisette) {
		// On récupère les paramètres du type de noisette.
		include_spip('inc/ncore_type_noisette');
		$champs = type_noisette_lire(
			$plugin,
			$type_noisette,
			'parametres',
			false,
			$stockage
		);

		// Et on leur associe des valeurs par défaut.
		include_spip('inc/saisies');
		$parametres = saisies_lister_valeurs_defaut($champs);

		// On charge les services de N-Core.
		// Ce sont ces fonctions qui aiguillent ou pas vers un service spécifique du plugin.
		include_spip('ncore/ncore');

		// Suivant que le conteneur est passé au format tableau ou id on complète les deux identifiants
		$id_conteneur = '';
		if (is_array($conteneur)) {
			// -- Vérification des index du conteneur.
			$conteneur = ncore_conteneur_verifier($plugin, $conteneur, $stockage);
			// -- Calcul de l'id
			if ($conteneur) {
				$id_conteneur = ncore_conteneur_identifier($plugin, $conteneur, $stockage);
			}
		} elseif (is_string($conteneur)) {
			$id_conteneur = $conteneur;
			$conteneur = ncore_conteneur_construire($plugin, $id_conteneur, $stockage);
		}

		// On initialise la description de la noisette à ajouter et en particulier on stocke le tableau et l'id du
		// conteneur pour simplifier les traitements par la suite.
		if ($id_conteneur) {
			// -- Initialisation par défaut
			$description = [
				'plugin'        => $plugin,
				'type_noisette' => $type_noisette,
				'conteneur'     => serialize($conteneur),
				'id_conteneur'  => $id_conteneur,
				'rang_noisette' => (int) $rang,
				'est_conteneur' => type_noisette_lire($plugin, $type_noisette, 'conteneur', false, $stockage),
				'parametres'    => serialize($parametres),
				'encapsulation' => 'defaut',
				'css'           => '',
				'css_saisies'   => serialize([]),
				'profondeur'    => 0
			];
			// -- Pour les noisettes conteneur pas de capsule englobante.
			if ($description['est_conteneur'] === 'oui') {
				$description['encapsulation'] = 'non';
			}

			// -- Pour une noisette incluse dans un conteneur noisette on calcule la profondeur.
			if (ncore_conteneur_est_noisette($plugin, $conteneur, $stockage)) {
				$description_conteneur = ncore_noisette_decrire($plugin, $conteneur['id_noisette'], $stockage);
				$description['profondeur'] = $description_conteneur['profondeur'] + 1;
			}

			// Complément à la description par défaut, spécifique au plugin utilisateur, si nécessaire.
			$description = ncore_noisette_completer_description($plugin, $description, 'ajouter', $stockage);

			// On récupère les noisettes déjà affectées au conteneur sous la forme d'un tableau indexé
			// par le rang de chaque noisette.
			$noisettes = ncore_noisette_lister($plugin, $id_conteneur, '', 'rang_noisette', $stockage);

			// On calcule le rang max déjà utilisé.
			$rang_max = $noisettes ? max(array_keys($noisettes)) : 0;

			if (!$rang or ($rang and ($rang > $rang_max))) {
				// Si, le rang est nul ou si il est strictement supérieur au rang_max, on positionne la noisette
				// à ajouter au rang max + 1.
				// En effet, si le rang est supérieur au rang max c'est que la nouvelle noisette est ajoutée
				// après les noisettes existantes, donc cela revient à insérer la noisette en fin de liste.
				// Postionner le rang à max + 1 permet d'éviter d'avoir des trous dans la liste des rangs.
				$description['rang_noisette'] = $rang_max + 1;
			} elseif ($rang <= $rang_max) {
				// Si le rang est non nul et inférieur ou égal au rang max c'est qu'on insère la noisette dans la liste
				// existante : il faut décaler d'un rang les noisettes de rang supérieur ou égal si elle existent pour
				// libérer la position de la nouvelle noisette.
				krsort($noisettes);
				foreach ($noisettes as $_rang => $_description) {
					if ($_rang >= $rang) {
						ncore_noisette_ranger($plugin, $_description, $_rang + 1, $stockage);
					}
				}
			}

			// La description de la nouvelle noisette est prête à être stockée à sa position.
			if ($noisette_ajoutee = ncore_noisette_stocker($plugin, $description, $stockage)) {
				// L'ajout s'est bien passé, on appelle, si besoin, une éventuelle fonction pour compléter l'action, soit
				// par service soit par pipeline.
				ncore_noisette_completer_action($plugin, $description, 'ajouter', $stockage);
			}
		}
	}

	return $noisette_ajoutee;
}

/**
 * Met à jour les paramètres éditables d'une noisette donnée.
 * La fonction contrôle la liste des champs modifiables.
 *
 * @api
 *
 * @uses ncore_noisette_decrire()
 * @uses ncore_noisette_completer_description()
 * @uses ncore_noisette_initialiser_parametrage()
 * @uses ncore_noisette_stocker()
 * @uses ncore_noisette_completer_action()
 *
 * @param string           $plugin        Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier ou
 *                                        un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param array|int|string $noisette      Identifiant de la noisette qui peut prendre soit la forme d'un entier ou d'une chaine unique, soit la forme
 *                                        d'un couple (id conteneur, rang).
 * @param array            $modifications Tableau des couples (champ, valeur) à mettre à jour pour la noisette spécifiée.
 *                                        La fonction contrôle la liste des champs éditables en filtrant les champs standard et éventuellement
 *                                        ceux spécifiquement définis par le plugin utilisateur.
 * @param null|string      $stockage      Identifiant du service de stockage à utiliser si précisé. Dans ce cas, ni celui du plugin
 *                                        ni celui de N-Core ne seront utilisés. En général, cet identifiant est le préfixe d'un plugin
 *                                        fournissant le service de stockage souhaité.
 *
 * @return bool `true` si la modification s'est bien passée, `false` sinon.
 */
function noisette_parametrer(string $plugin, $noisette, array $modifications, ?string $stockage = '') : bool {
	// Initialisation du retour
	$noisette_parametree = false;

	// On charge les services de N-Core.
	// Ce sont ces fonctions qui aiguillent ou pas vers un service spécifique du plugin.
	include_spip('ncore/ncore');

	// L'identifiant d'une noisette peut être fourni de deux façons :
	// - par une valeur unique, celle créée lors de l'ajout et qui peut-être un entier (id d'une table SPIP) ou
	//   une chaine unique par exemple générée par uniqid().
	// - ou par un tableau à deux entrées fournissant le conteneur et le rang dans le conteneur
	//   (qui est unique pour un conteneur donné).
	if (!empty($noisette) and (is_string($noisette) or is_numeric($noisette) or is_array($noisette))) {
		// On récupère la description complète de la noisette avant de modifier les champs éditables spécifiés.
		$description = ncore_noisette_decrire($plugin, $noisette, $stockage);

		// On contrôle les champs éditables et on met à jour la description de la noisette.
		$parametres = ncore_noisette_initialiser_parametrage($plugin, $description['est_conteneur'], $stockage);
		$modifications = array_intersect_key($modifications, array_flip($parametres));
		$description = array_merge($description, $modifications);

		// Complément à la description par défaut, spécifique au plugin utilisateur, si nécessaire.
		$description = ncore_noisette_completer_description($plugin, $description, 'parametrer', $stockage);

		// La description est prête à être stockée en remplacement de l'existante.
		if (ncore_noisette_stocker($plugin, $description, $stockage)) {
			$noisette_parametree = true;

			// La mise à jour s'est bien passée, on appelle, si besoin, une éventuelle fonction pour compléter
			// l'action, soit par service soit par pipeline.
			ncore_noisette_completer_action($plugin, $description, 'parametrer', $stockage);
		}
	}

	return $noisette_parametree;
}

/**
 * Supprime une noisette donnée du conteneur auquel elle est associée et, si cette noisette est un conteneur,
 * le vide de ses noisettes au préalable.
 * La fonction met à jour les rangs des autres noisettes si nécessaire.
 *
 * @api
 *
 * @uses ncore_noisette_decrire()
 * @uses ncore_noisette_destocker()
 * @uses ncore_noisette_lister()
 * @uses ncore_noisette_ranger()
 * @uses ncore_noisette_completer_action()
 *
 * @param string           $plugin   Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier ou
 *                                   un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param array|int|string $noisette Identifiant de la noisette qui peut prendre soit la forme d'un entier ou d'une chaine unique, soit la forme
 *                                   d'un couple (id conteneur, rang).
 * @param null|string      $stockage Identifiant du service de stockage à utiliser si précisé. Dans ce cas, ni celui du plugin
 *                                   ni celui de N-Core ne seront utilisés. En général, cet identifiant est le préfixe d'un plugin
 *                                   fournissant le service de stockage souhaité.
 *
 * @return bool `true` si la suppression s'est bien passée, `false` sinon.
 */
function noisette_supprimer(string $plugin, $noisette, ?string $stockage = '') : bool {
	// Initialisation du retour
	$noisette_supprimee = false;

	// On charge les services de N-Core.
	// Ce sont ces fonctions qui aiguillent ou pas vers un service spécifique du plugin.
	include_spip('ncore/ncore');

	// L'identifiant d'une noisette peut être fourni de deux façons :
	// - par une valeur unique, celle créée lors de l'ajout et qui peut-être un entier (id d'une table SPIP) ou
	//   une chaine unique par exemple générée par uniqid().
	// - ou par un tableau à deux entrées fournissant le conteneur et le rang dans le conteneur
	//   (qui est unique pour un conteneur donné).
	if (
		!empty($noisette)
		and (
			is_string($noisette)
			or is_numeric($noisette)
			or is_array($noisette)
		)
	) {
		// Avant de supprimer la noisette on sauvegarde sa description.
		// Cela permet de conserver le rang et l'id du conteneur indépendamment de l'identifiant
		// utilisé pour spécifier la noisette.
		$description = ncore_noisette_decrire($plugin, $noisette, $stockage);

		// Si la noisette est de type conteneur, il faut d'abord vider le conteneur qu'elle représente
		// et ce de façon récursive. La récursivité est gérée par la fonction de service ncore_conteneur_destocker().
		$conteneur_destocke = true;
		if ($description['est_conteneur'] === 'oui') {
			// Inutile de redéfinir un conteneur car la description de la noisette contient les deux champs
			// essentiels, à savoir, type_noisette et id_noisette.
			$conteneur_destocke = ncore_conteneur_destocker($plugin, $description, $stockage);
		}

		// Suppression de la noisette. On passe la description complète ce qui permet à la fonction de
		// destockage de choisir la méthode d'identification la plus adaptée.
		if ($conteneur_destocke and ncore_noisette_destocker($plugin, $description, $stockage)) {
			// On récupère les noisettes restant affectées au conteneur sous la forme d'un tableau indexé par rang.
			$autres_noisettes = ncore_noisette_lister(
				$plugin,
				$description['id_conteneur'],
				'',
				'rang_noisette',
				$stockage
			);

			// Si il reste des noisettes, on tasse d'un rang les noisettes qui suivaient la noisette supprimée.
			if ($autres_noisettes) {
				// On lit les noisettes restantes dans l'ordre décroissant pour éviter d'écraser une noisette.
				ksort($autres_noisettes);
				foreach ($autres_noisettes as $_rang => $_autre_description) {
					if ($_rang > $description['rang_noisette']) {
						ncore_noisette_ranger(
							$plugin,
							$_autre_description,
							$_autre_description['rang_noisette'] - 1,
							$stockage
						);
					}
				}
			}

			$noisette_supprimee = true;

			// La suppression s'est bien passée, on appelle, si besoin, une éventuelle fonction pour compléter
			// l'action, soit par service soit par pipeline.
			ncore_noisette_completer_action($plugin, $description, 'supprimer', $stockage);
		}
	}

	return $noisette_supprimee;
}

/**
 * Retourne, pour une noisette donnée, la description complète ou seulement un champ précis.
 * Les champs textuels peuvent subir un traitement typo si demandé.
 *
 * @api
 *
 * @uses ncore_noisette_decrire()
 *
 * @param string           $plugin       Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier ou
 *                                       un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param array|int|string $noisette     Identifiant de la noisette qui peut prendre soit la forme d'un entier ou d'une chaine unique, soit la forme
 *                                       d'un couple (id conteneur, rang).
 * @param null|string      $information  Information spécifique à retourner ou vide pour retourner toute la description.
 * @param null|bool        $traiter_typo Indique si les données textuelles doivent être retournées brutes ou si elles doivent être traitées
 *                                       en utilisant la fonction typo. Par défaut l'indicateur vaut `false`.
 *                                       Les champs sérialisés sont eux toujours désérialisés.
 *                                       Pour l'instant il n'y a pas de champ textuel directement associé à une noisette.
 * @param null|string      $stockage     Identifiant du service de stockage à utiliser si précisé. Dans ce cas, ni celui du plugin
 *                                       ni celui de N-Core ne seront utilisés. En général, cet identifiant est le préfixe d'un plugin
 *                                       fournissant le service de stockage souhaité.
 *
 * @return array|int|string La description complète ou champ précis demandé pour une noisette donnée. Les champs
 *                          de type tableau sont systématiquement désérialisés et si demandé, les champs textuels peuvent être
 *                          traités avec la fonction typo().
 */
function noisette_lire(string $plugin, $noisette, ?string $information = '', ?bool $traiter_typo = false, ?string $stockage = '') {
	// On indexe le tableau des descriptions par le plugin appelant en cas d'appel sur le même hit
	// par deux plugins différents.
	// En outre, on gère un tableau par type d'identification, id noisette ou couple (id conteneur, rang).
	static $description_noisette_par_id = [];
	static $description_noisette_par_rang = [];

	// Initialisation de la description en sortie.
	$noisette_lue = [];

	if (
		!empty($noisette)
		and (
			is_string($noisette)
			or is_numeric($noisette)
			or is_array($noisette)
		)
	) {
		// On charge les services de N-Core.
		// Ce sont ces fonctions qui aiguillent ou pas vers un service spécifique du plugin.
		include_spip('ncore/ncore');

		// On vérifie si la noisette est valide et si la description n'a pas déjà été enregistrée dans le tableau adéquat.
		$description_existe = false;
		$noisette_invalide = false;
		if (!is_array($noisette)) {
			$description_existe = isset($description_noisette_par_id[$plugin][$traiter_typo][$noisette]);
		} elseif (isset($noisette['id_conteneur'], $noisette['rang_noisette'])) {
			$description_existe = isset($description_noisette_par_rang[$plugin][$traiter_typo][$noisette['id_conteneur']][$noisette['rang_noisette']]);
		} else {
			$noisette_invalide = true;
		}

		if (!$noisette_invalide) {
			if (!$description_existe) {
				// Lecture de toute la configuration de la noisette: les données retournées sont brutes.
				$description = ncore_noisette_decrire($plugin, $noisette, $stockage);

				if ($description) {
					// Traitements des champs textuels : aucun champ textuel à traiter dans la description par défaut.
					if ($traiter_typo) {
						$description = ncore_noisette_traiter_typo($plugin, $description, $stockage);
					}

					// Traitements des champs tableaux sérialisés si nécessaire
					$description['parametres'] = unserialize($description['parametres']);
					$description['conteneur'] = unserialize($description['conteneur']);
					$description['css_saisies'] = unserialize($description['css_saisies']);
				}

				// Sauvegarde de la description de la noisette pour une consultation ultérieure dans le même hit
				// en suivant le type d'identification.
				if (!is_array($noisette)) {
					$description_noisette_par_id[$plugin][$traiter_typo][$noisette] = $description;
				} else {
					$description_noisette_par_rang[$plugin][$traiter_typo][$noisette['id_conteneur']][$noisette['rang_noisette']] = $description;
				}
			}

			if ($information) {
				if ((!is_array($noisette) and isset($description_noisette_par_id[$plugin][$traiter_typo][$noisette][$information]))
					or (is_array($noisette)
						and isset($description_noisette_par_rang[$plugin][$traiter_typo][$noisette['id_conteneur']][$noisette['rang_noisette']][$information]))) {
					$noisette_lue = is_array($noisette)
						? $description_noisette_par_rang[$plugin][$traiter_typo][$noisette['id_conteneur']][$noisette['rang_noisette']][$information]
						: $description_noisette_par_id[$plugin][$traiter_typo][$noisette][$information];
				} else {
					$noisette_lue = '';
				}
			} else {
				$noisette_lue = is_array($noisette)
					? $description_noisette_par_rang[$plugin][$traiter_typo][$noisette['id_conteneur']][$noisette['rang_noisette']]
					: $description_noisette_par_id[$plugin][$traiter_typo][$noisette];
			}
		}
	}

	return $noisette_lue;
}

/**
 * Déplace une noisette donnée au sein d’un même conteneur ou dans un autre conteneur.
 * La fonction met à jour les rangs des autres noisettes si nécessaire.
 *
 * @api
 *
 * @uses ncore_noisette_decrire()
 * @uses ncore_conteneur_verifier()
 * @uses ncore_conteneur_identifier()
 * @uses ncore_noisette_lister()
 * @uses ncore_conteneur_construire()
 * @uses ncore_conteneur_est_noisette()
 * @uses ncore_noisette_changer_conteneur()
 * @uses ncore_noisette_ranger()
 * @uses ncore_noisette_completer_action()
 *
 * @param string           $plugin                Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier ou
 *                                                un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param array|int|string $noisette              Identifiant de la noisette qui peut prendre soit la forme d'un entier ou d'une chaine unique, soit la forme
 *                                                d'un couple (id conteneur, rang).
 * @param array|string     $conteneur_destination Identifiant du conteneur destination qui prend soit la forme d'un tableau soit celui d'un id.
 * @param int              $rang_destination      Entier représentant le rang où repositionner la noisette dans le squelette contextualisé.
 * @param null|string      $stockage              Identifiant du service de stockage à utiliser si précisé.
 *
 * @return bool Renvoie `false` si la noisette n'est pas déplacée à cause d'une erreur de traitement ou parce que le
 *              rang destination est identique au rang origine, sinon `true`.
 */
function noisette_deplacer(string $plugin, $noisette, $conteneur_destination, int $rang_destination, ?string $stockage = '') : bool {
	// Initialisation du retour
	$noisette_deplacee = false;

	// On charge les services de N-Core.
	// Ce sont ces fonctions qui aiguillent ou pas vers un service spécifique du plugin.
	include_spip('ncore/ncore');

	// L'identifiant d'une noisette peut être fourni de deux façons :
	// - par une valeur unique, celle créée lors de l'ajout et qui peut-être un entier (id d'une table SPIP) ou
	//   une chaine unique par exemple générée par uniqid().
	// - ou par un tableau à deux entrées fournissant le conteneur et le rang
	//   (qui est unique pour un conteneur donné).
	if (
		!empty($noisette)
		and (
			is_string($noisette)
			or is_numeric($noisette)
			or is_array($noisette)
		)
	) {
		// Avant de déplacer la noisette on sauvegarde sa description, son id conteneur et son rang.
		$description = ncore_noisette_decrire($plugin, $noisette, $stockage);
		$id_conteneur_origine = $description['id_conteneur'];
		$rang_origine = $description['rang_noisette'];

		// On détermine l'id du conteneur destination en fonction du mode d'identification fourni par l'argument.
		$id_conteneur_destination = '';
		if (is_array($conteneur_destination)) {
			// -- Vérification des index du conteneur.
			$conteneur_destination = ncore_conteneur_verifier($plugin, $conteneur_destination, $stockage);
			// -- Calcul de l'id
			if ($conteneur_destination) {
				$id_conteneur_destination = ncore_conteneur_identifier(
					$plugin,
					$conteneur_destination,
					$stockage
				);
			}
		} elseif (is_string($conteneur_destination)) {
			$id_conteneur_destination = $conteneur_destination;
		}

		// Si le conteneur destination est différent du conteneur origine, la première opération consiste à
		// transférer la noisette en fin du conteneur destination, ce qui est toujours possible.
		// Ensuite on tasse le conteneur d'origine.
		if ($id_conteneur_destination) {
			if ($id_conteneur_destination != $id_conteneur_origine) {
				// On recherche le dernier rang utilisé dans le conteneur destination et on se positionne après.
				$rangs = ncore_noisette_lister(
					$plugin,
					$id_conteneur_destination,
					'rang_noisette',
					'id_noisette',
					$stockage
				);
				$rang_conteneur_destination = $rangs ? max($rangs) + 1 : 1;

				// Pour éviter que chaque plugin ait à gérer la profondeur, N-Core calcule la profondeur de la noisette à son
				// nouvel emplacement avant de déplacer celle-ci et la fournit à la fonction de changement de conteneur.
				if (!is_array($conteneur_destination)) {
					$conteneur_destination = ncore_conteneur_construire($plugin, $id_conteneur_destination, $stockage);
				}
				if (ncore_conteneur_est_noisette($plugin, $conteneur_destination, $stockage)) {
					$description_conteneur = ncore_noisette_decrire($plugin, $conteneur_destination['id_noisette'], $stockage);
					$profondeur_destination = $description_conteneur['profondeur'] + 1;
				} else {
					$profondeur_destination = 0;
				}

				// On transfère la noisette vers le conteneur destination à la position calculée (max + 1).
				$description = ncore_noisette_changer_conteneur(
					$plugin,
					$description,
					$id_conteneur_destination,
					$rang_conteneur_destination,
					$profondeur_destination,
					$stockage
				);

				// Il faut maintenant tasser les noisettes du conteneur d'origine qui a perdu une noisette.
				// -- On récupère les noisettes restant affectées au conteneur origine sous la forme d'un tableau
				//    indexé par rang.
				$autres_noisettes = ncore_noisette_lister(
					$plugin,
					$id_conteneur_origine,
					'',
					'rang_noisette',
					$stockage
				);

				// Si il reste des noisettes, on tasse d'un rang les noisettes qui suivaient la noisette supprimée.
				if ($autres_noisettes) {
					// On lit les noisettes restantes dans l'ordre décroissant pour éviter d'écraser une noisette.
					ksort($autres_noisettes);
					foreach ($autres_noisettes as $_rang => $_autre_description) {
						if ($_rang > $rang_origine) {
							ncore_noisette_ranger($plugin, $_autre_description, $_autre_description['rang_noisette'] - 1, $stockage);
						}
					}
				}

				// Le rang origine devient donc le nouveau rang de la noisette dans le conteneur destination.
				$rang_origine = $description['rang_noisette'];
			}

			// A partir de là, le déplacement est un déplacement à l'intérieur d'un conteneur.
			// Si les rangs origine et destination sont identiques on ne fait rien !
			if ($rang_destination != $rang_origine) {
				// On récupère les noisettes affectées au même conteneur sous la forme d'un tableau indexé par le rang.
				$noisettes = ncore_noisette_lister(
					$plugin,
					$description['id_conteneur'],
					'',
					'rang_noisette',
					$stockage
				);

				// On vérifie que le rang destination est bien compris entre 1 et le rang max, sinon on le force à l'une
				// des bornes.
				$rang_destination = max(1, $rang_destination);
				$rang_max = $noisettes ? max(array_keys($noisettes)) : 0;
				$rang_destination = min($rang_max, $rang_destination);

				// Suivant la position d'origine et de destination de la noisette déplacée on trie les noisettes
				// du conteneur.
				if ($rang_destination < $rang_origine) {
					krsort($noisettes);
				} else {
					ksort($noisettes);
				}

				// On déplace les noisettes impactées à l'exception de la noisette concernée par le déplacement
				// afin de créer une position libre.
				foreach ($noisettes as $_rang => $_description) {
					if ($rang_destination < $rang_origine) {
						// On "descend" les noisettes du rang destination au rang origine non compris.
						if (($_rang >= $rang_destination) and ($_rang < $rang_origine)) {
							ncore_noisette_ranger($plugin, $_description, $_rang + 1, $stockage);
						}
					} elseif (($_rang <= $rang_destination) and ($_rang > $rang_origine)) {
						// On "remonte" les noisettes du rang destination au rang origine non compris.
						ncore_noisette_ranger($plugin, $_description, $_rang - 1, $stockage);
					}
				}

				// On positionne le rang de la noisette à déplacer au rang destination.
				// On met le rang à zéro pour indiquer que l'emplacement d'origine n'est plus occupé par la
				// noisette et qu'il ne faut pas le supprimer lors du rangement.
				$description['rang_noisette'] = 0;
				if (ncore_noisette_ranger($plugin, $description, $rang_destination, $stockage)) {
					$noisette_deplacee = true;
				}
			}

			// Le déplacement s'est bien passé, on appelle, si besoin, une éventuelle fonction pour compléter
			// l'action, soit par service soit par pipeline.
			ncore_noisette_completer_action($plugin, $description, 'deplacer', $stockage);
		}
	}

	return $noisette_deplacee;
}

/**
 * Duplique une noisette donnée dans un conteneur destination et, si cette noisette est un conteneur, duplique aussi
 * les noisettes contenues de façon récursive.
 *
 * @api
 *
 * @uses ncore_noisette_decrire()
 * @uses noisette_ajouter()
 * @uses noisette_parametrer()
 * @uses ncore_noisette_lister()
 * @uses noisette_dupliquer()
 * @uses ncore_noisette_completer_action()
 *
 * @param string           $plugin      Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier ou
 *                                      un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param array|int|string $noisette    Identifiant de la noisette qui peut prendre soit la forme d'un entier ou d'une chaine unique, soit la forme
 *                                      d'un couple (id conteneur, rang).
 * @param array|string     $conteneur   Identifiant du conteneur destination qui prend soit la forme d'un tableau soit celui d'un id.
 * @param null|int         $rang        Rang dans le conteneur destination où insérer la noisette dupliquée. Si l'argument n'est pas fourni ou
 *                                      est égal à 0 on insère la noisette en fin de conteneur.
 * @param null|array       $parametrage Tableau indiquant les champs éditables de la noisette source à copier dans la noisette dupliquée.
 *                                      Si null, indique de copier tous les champs éditables, si tableau vide, aucun
 * @param null|string      $stockage    Identifiant du service de stockage à utiliser si précisé. Dans ce cas, ni celui du plugin
 *                                      ni celui de N-Core ne seront utilisés. En général, cet identifiant est le préfixe d'un plugin
 *                                      fournissant le service de stockage souhaité.
 *
 * @return bool `true` si la duplication s'est bien passée, `false` sinon.
 */
function noisette_dupliquer(string $plugin, $noisette, $conteneur, ?int $rang = 0, ?array $parametrage = null, ?string $stockage = '') : bool {
	// Initialisation du retour
	$noisette_dupliquee = false;

	// On charge les services de N-Core.
	// Ce sont ces fonctions qui aiguillent ou pas vers un service spécifique du plugin.
	include_spip('ncore/ncore');

	// L'identifiant d'une noisette peut être fourni de deux façons :
	// - par une valeur unique, celle créée lors de l'ajout et qui peut-être un entier (id d'une table SPIP) ou
	//   une chaine unique par exemple générée par uniqid().
	// - ou par un tableau à deux entrées fournissant le conteneur et le rang dans le conteneur
	//   (qui est unique pour un conteneur donné).
	if (
		!empty($noisette)
		and (
			is_string($noisette)
			or is_numeric($noisette)
			or is_array($noisette)
		)
	) {
		// Avant de dupliquer la noisette on sauvegarde sa description car il faudra utiliser ses paramètres
		// d'affichage, d'encapsulation et de style pour la duplication.
		$description = ncore_noisette_decrire($plugin, $noisette, $stockage);

		// On ajoute la noisette à la fin du conteneur destination : la noisette est créée par défaut.
		if ($id_noisette = noisette_ajouter($plugin, $description['type_noisette'], $conteneur, $rang, $stockage)) {
			// Suivant le paramétrage demandé on copie les champs idoines de la noisette source.
			if ($parametrage === null) {
				$parametrage = ncore_noisette_initialiser_parametrage($plugin, $description['est_conteneur'], $stockage);
			}
			if ($parametrage) {
				$modifications = [];
				foreach ($parametrage as $_champ) {
					$modifications[$_champ] = $description[$_champ];
				}

				if ($modifications) {
					noisette_parametrer($plugin, $id_noisette, $modifications, $stockage);
				}
			}

			// Si la noisette est de type conteneur, il faut aussi dupliquer les noisettes éventuellement contenues
			// et ce de façon récursive.
			if ($description['est_conteneur'] === 'oui') {
				// On récupère les noisettes incluses dans la noisette origine qui est un conteneur.
				$autres_noisettes = ncore_noisette_lister(
					$plugin,
					['type_noisette' => $description['type_noisette'], 'id_noisette' => $description['id_noisette']],
					'',
					'rang_noisette',
					$stockage
				);

				// Si il reste des noisettes, on tasse d'un rang les noisettes qui suivaient la noisette supprimée.
				if ($autres_noisettes) {
					// On calcule l'id conteneur de la noisette conteneur venant d'être dupliquée pour y dupliquer les
					// noisettes incluses.
					$conteneur_noisette_dupliquee = [
						'type_noisette' => $description['type_noisette'],
						'id_noisette'   => $id_noisette
					];

					// On lit les noisettes incluses et on appelle la fonction dupliquer récursivement.
					ksort($autres_noisettes);
					foreach ($autres_noisettes as $_rang => $_autre_description) {
						noisette_dupliquer(
							$plugin,
							$_autre_description['id_noisette'],
							$conteneur_noisette_dupliquee,
							$_rang,
							$parametrage,
							$stockage
						);
					}
				}
			}

			$noisette_dupliquee = true;

			// La duplication s'est bien passée, on appelle, si besoin, une éventuelle fonction pour compléter
			// l'action, soit par service soit par pipeline.
			// On lit la description de la noisette issue de la duplication. Si c'est un conteneur on ne renvoie que
			// le conteneur créé pas les noisettes incluses.
			$noisette_creee = ncore_noisette_decrire($plugin, $id_noisette, $stockage);
			ncore_noisette_completer_action($plugin, $noisette_creee, 'dupliquer', $stockage);
		}
	}

	return $noisette_dupliquee;
}

/**
 * Renvoie une liste de descriptions de noisettes appartenant à un conteneur donné ou pas et éventuellement filtrée
 * sur certains champs.
 * Le tableau retourné est indexé soit par identifiant de noisette soit par identifiant du conteneur et rang de la
 * noisette.
 *
 * @api
 *
 * @uses ncore_conteneur_verifier()
 * @uses ncore_noisette_lister()
 *
 * @param string            $plugin    Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                                     ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param null|array|string $conteneur Tableau descriptif du conteneur ou identifiant du conteneur ou vide si on souhaite adresser tous les
 *                                     conteneurs.
 * @param null|string       $cle       Champ de la description d'une noisette servant d'index du tableau. On utilisera soit `id_noisette`
 *                                     soit `rang_noisette` (défaut).
 * @param null|array        $filtres
 *                                     Tableau associatif `[champ] = valeur` de critères de filtres sur les descriptions de types de noisette.
 *                                     Le seul opérateur possible est l'égalité.
 * @param null|string       $stockage  Identifiant du service de stockage à utiliser si précisé.
 *
 * @return array Tableau des descriptions des noisettes du conteneur indexé par le champ fourni en argument (par défaut le rang).
 */
function noisette_repertorier(string $plugin, $conteneur = [], ?string $cle = 'rang_noisette', ?array $filtres = [], ?string $stockage = '') : array {
	// On indexe le tableau des noisettes par le plugin appelant en cas d'appel sur le même hit
	// par deux plugins différents et aussi par la clé d'indexation.
	static $noisettes = [];

	if (!isset($noisettes[$plugin][$cle])) {
		// On charge l'API de N-Core.
		// Ce sont ces fonctions qui aiguillent ou pas vers une fonction spécifique du service.
		include_spip('ncore/ncore');

		// On récupère la description complète de toutes les noisettes ou des noisettes appartenant au conteneur
		// spécifié.
		if ($conteneur) {
			// On vérifie le conteneur avant de l'utiliser ce qui évite aux plugins utilisateurs de le faire.
			$conteneur = ncore_conteneur_verifier($plugin, $conteneur, $stockage);
		}
		$noisettes[$plugin][$cle] = ncore_noisette_lister($plugin, $conteneur, '', $cle, $stockage);
	}

	// Application des filtres éventuellement demandés en argument de la fonction
	$noisettes_filtrees = $noisettes[$plugin][$cle];
	if ($filtres) {
		foreach ($noisettes_filtrees as $_noisette => $_description) {
			foreach ($filtres as $_critere => $_valeur) {
				if (isset($_description[$_critere]) and ($_description[$_critere] != $_valeur)) {
					unset($noisettes_filtrees[$_noisette]);
					break;
				}
			}
		}
	}

	return $noisettes_filtrees;
}
