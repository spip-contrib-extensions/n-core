<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/n-core.git

return [

	// E
	'erreur_noisette_compilation_nok' => 'Une erreur s’est produite pendant la compilation de la noisette.',
	'erreur_noisette_edition_interdite' => 'Il n’est pas possible d’éditer les paramètres de la noisette.',
	'erreur_noisette_edition_nok' => 'Une erreur s’est produite pendant la mise à jour de la noisette.',
	'erreur_type_noisette_inactif' => 'Le type de noisette @type_noisette@ est inactivé car le ou les plugins suivants sont désactivés : @plugins@.',

	// P
	'preview_conteneur' => 'Balise englobante : @balise@',

	// S
	'saisie_affichage_legende' => 'Affichage',
	'saisie_avance_legende' => 'Avancé',
	'saisie_conteneur_balise_label' => 'Balise matérialisant le conteneur',
	'saisie_contenu_legende' => 'Contenu',
	'saisie_css_explication' => 'Vous pouvez ajouter des styles complémentaires à la noisette.',
	'saisie_css_label' => 'Classes CSS',
	'saisie_encapsulation_defaut_option' => 'Utiliser le mode par défaut configuré pour le noiZetier <em>(@defaut@)</em>',
	'saisie_encapsulation_label' => 'Encapsulation',
	'saisie_encapsulation_non_info' => 'sans capsule',
	'saisie_encapsulation_non_option' => 'Ne jamais encapsuler la noisette',
	'saisie_encapsulation_oui_info' => 'avec capsule',
	'saisie_encapsulation_oui_option' => 'Inclure la noisette dans une capsule',

	// T
	'type_noisette_categorie_defaut_description' => 'Type de noisette non affecté à une catégorie spécifique',
	'type_noisette_categorie_defaut_label' => 'Autres types de noisette',
	'type_noisette_conteneur_description' => 'Conteneur pouvant accueillir des noisettes',
	'type_noisette_conteneur_titre' => 'Bloc conteneur',
	'type_noisette_environnement_description' => 'Affichage des variables d’environnement, balise <code>#ENV</code>, à des fins de debug',
	'type_noisette_environnement_titre' => 'Environnement SPIP',
];
