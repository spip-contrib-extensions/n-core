<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/ncore-paquet-xml-n-core?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// N
	'ncore_description' => 'Block management framework. This plugin provides APIs and mechanisms for managing and compiling blocks.',
	'ncore_slogan' => 'Core management for blocks',
];
