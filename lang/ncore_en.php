<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/ncore-n-core?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// E
	'erreur_noisette_compilation_nok' => 'An error occurred while compiling the block.',
	'erreur_noisette_edition_interdite' => 'It is not possible to edit block parameters.',
	'erreur_noisette_edition_nok' => 'An error occurred while updating the block.',
	'erreur_type_noisette_inactif' => 'The @type_noisette@ block type is inactivated because the following plugin(s) are disabled: @plugins@.',

	// P
	'preview_conteneur' => 'Wrapping tag : @balise@',

	// S
	'saisie_affichage_legende' => 'Style',
	'saisie_avance_legende' => 'Advanced',
	'saisie_conteneur_balise_label' => 'Container tag',
	'saisie_contenu_legende' => 'Content',
	'saisie_css_explication' => 'You can add additional styles to the block.',
	'saisie_css_label' => 'CSS classes',
	'saisie_encapsulation_defaut_option' => 'Use the default mode configured for the noiZetier <em>(@defaut@)</em>.',
	'saisie_encapsulation_label' => 'Block wrapping',
	'saisie_encapsulation_non_info' => 'without wrapper',
	'saisie_encapsulation_non_option' => 'Never wrap the block',
	'saisie_encapsulation_oui_info' => 'with wrapper',
	'saisie_encapsulation_oui_option' => 'Wrap the block',

	// T
	'type_noisette_categorie_defaut_description' => 'Block type not assigned to a specific category',
	'type_noisette_categorie_defaut_label' => 'Others block types',
	'type_noisette_conteneur_description' => 'Block container',
	'type_noisette_conteneur_titre' => 'Zone container',
	'type_noisette_environnement_description' => 'Display environment variables, tag <code>#ENV</code>, for debugging purposes',
	'type_noisette_environnement_titre' => 'SPIP environment variables',
];
