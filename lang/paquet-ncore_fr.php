<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/n-core.git

return [

	// N
	'ncore_description' => 'Framework de gestion de noisettes. Ce plugin fournit les API et les mécanismes permettant de gérer et de compiler les noisettes.',
	'ncore_slogan' => 'Un cœur fondant aux noisettes',
];
