<?php
/**
 * Ce fichier contient la configuration des caches de N-Core basés sur l'API de Cache Factory.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie la configuration spécifique des caches de N-Core.
 *
 * @param string $plugin Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier ou
 *                       un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 *
 * @return array Tableau de la configuration des caches du plugin N-Core.
 */
function ncore_cache_configurer(string $plugin) : array {
	// Initialisation du tableau de configuration avec les valeurs par défaut du plugin Cache.
	return [
		'stockage' => [
			'racine'          => '_DIR_CACHE',
			'sous_dossier'    => true,
			'nom_obligatoire' => ['objet', 'fonction'],
			'nom_facultatif'  => [],
			'extension'       => '.php',
			'securisation'    => true,
			'serialisation'   => true,
			'separateur'      => '-',
			'conservation'    => 0
		],
	];
}
