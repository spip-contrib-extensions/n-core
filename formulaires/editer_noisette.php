<?php
/**
 * Gestion du formulaire générique d'édition d'une noisette.
 *
 * @package SPIP\NCORE\NOISETTE\UI
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!function_exists('autoriser')) {
	// si on utilise le formulaire dans le public
	include_spip('inc/autoriser');
}

/**
 * Construction des saisies : les saisies du formulaire sont totalement définies en PHP (le fichier HTML est vide).
 * Par défaut, N-Core range les saisies dans trois groupes (fieldsets), à savoir, Contenu, Affichage et Avancé qui
 * ne sont affichés que si il contienne au moins une saisie incluse.
 *
 * @uses noisette_lire()
 * @uses type_noisette_lire()
 * @uses ncore_noisette_initialiser_encapsulation()
 *
 * @param string      $plugin      Identifiant qui permet de distinguer le module appelant. Pour un plugin, le plus pertinent
 *                                 est d'utiliser le préfixe.
 * @param int|string  $id_noisette Identifiant unique de la noisette.
 * @param null|string $redirect    URL de redirection en sortie du formulaire.
 * @param null|string $stockage    Identifiant du service de stockage à utiliser si précisé.
 *
 * @return array Tableau des saisies constitutives du formulaire
 */
function formulaires_editer_noisette_saisies_dist(string $plugin, $id_noisette, ?string $redirect = '', ?string $stockage = '') : array {
	// On sauvegarde les saisies de la noisette en statique
	static $saisies = [];

	// Dans cette fonction on définit les saisies mais on ne charge jamais les valeurs par défaut qui
	// seront positionnées par le mécanisme de Saisies à partir des valeurs chargées (fonction _charger du formulaire).
	if (
		!$saisies
		and include_spip('ncore/ncore')
		and ($id_noisette = ncore_noisette_verifier_identifiant($plugin, $id_noisette, $stockage))
		and autoriser('editer', 'noisette', $id_noisette, null, ['plugin' => $plugin, 'stockage' => $stockage])
	) {
		// En premier lieu on insère 2 hidden pour stocker le type de noisette, l'indicateur de noisette conteneur
		$saisies[] = [
			'saisie'  => 'hidden',
			'options' => [
				'nom' => 'type_noisette',
			]
		];
		$saisies[] = [
			'saisie'  => 'hidden',
			'options' => [
				'nom' => 'est_conteneur',
			]
		];

		// On construit par défaut trois groupes de saisies proposés dans cet ordre :
		// - celui des paramètres de contenu (id contenu)
		// - celui des paramètres d'affichage qui contient toujours à minima les classes CSS (id affichage)
		// - celui des paramètres avancés (id avance) qui peut contenir l'encpasulation suivant le mode utilisé (auto ou non)
		// - avec la possibilité d'en rajouter d'autres après celui de contenu
		// -- Récupérer la description de la noisette
		include_spip('inc/ncore_noisette');
		$noisette = noisette_lire(
			$plugin,
			$id_noisette,
			'',
			false,
			$stockage
		);
		// -- Récupérer la description des saisies et des types de paramètres
		include_spip('inc/ncore_type_noisette');
		$type_noisette = type_noisette_lire(
			$plugin,
			$noisette['type_noisette'],
			'',
			false,
			$stockage
		);

		$groupes = $type_noisette['groupes'];
		// On indexe le tableau des saisies par le nom de la saisie (pas de fonction Saisies pour cela)
		$champs = [];
		foreach ($type_noisette['parametres'] as $_champ) {
			$champs[$_champ['options']['nom']] = $_champ;
		}

		// Initialiser les groupes avec leur saisies respectives
		// -- Initialiser le tableau des saisies par groupe en distinguant les groupes par défaut et les persos
		$groupes_defaut = ['contenu', 'affichage', 'avance'];
		foreach ($groupes_defaut as $_groupe) {
			$saisies_par_groupe_defaut[$_groupe] = [];
		}
		$saisies_par_groupe = $saisies_par_groupe_perso = [];
		// -- Répartir les saisies dans les groupes
		if ($champs) {
			if ($groupes) {
				foreach ($groupes as $_groupe => $_parametres) {
					if (in_array($_groupe, $groupes_defaut)) {
						$saisies_par_groupe_defaut[$_groupe] = array_intersect_key($champs, array_flip($_parametres));
					} else {
						$saisies_par_groupe_perso[$_groupe] = array_intersect_key($champs, array_flip($_parametres));
					}
				}
			} else {
				$saisies_par_groupe_defaut['contenu'] = $champs;
			}
		}

		// Groupe des saisies contenu
		// -- On insère le groupe de saisies dans la variable finale
		$saisies_par_groupe['contenu'] = $saisies_par_groupe_defaut['contenu'];

		// Groupe des saisies perso
		// -- on merge les saisies perso juste après celles du contenu car il y a plus de chance que ce soit un
		//    sous découpage de contenu.
		if ($saisies_par_groupe_perso) {
			$saisies_par_groupe = array_merge($saisies_par_groupe, $saisies_par_groupe_perso);
		}

		// Groupe des saisies affichage
		// -- Compléter le groupe des saisies d'affichage par la saisie des css appliqués à la capsule ou au conteneur
		//    Toujours affiché sauf si le mode d'encapsulation automatique a été débrayé
		$saisie_css = $type_noisette['css_saisies'];
		if (
			include_spip('ncore_fonctions')
			and !_NCORE_ENCAPSULATION_AUTO
		) {
			$saisie_css['options']['afficher_si'] = '(@est_conteneur@ == "oui") || (@encapsulation@ == "oui") || ((@encapsulation@ == "defaut") && (@config:noizetier/encapsulation_noisette@ == "on"))';
		}
		$saisies_par_groupe_defaut['affichage'] = array_merge($saisies_par_groupe_defaut['affichage'], $saisie_css);

		// -- On insère le groupe de saisies dans la variable finale
		$saisies_par_groupe['affichage'] = $saisies_par_groupe_defaut['affichage'];

		// Groupe des saisies avancé
		// -- si le mode d'encapsulation automatique a été débrayé, on ajoute une saisie au groupe avancé :
		//    le mode d'encapsulation propre de la noisette (sauf pour les conteneurs qui n'ont pas de capsule).
		$saisie_encapsulation = [];
		if (
			!_NCORE_ENCAPSULATION_AUTO
			and $noisette['est_conteneur'] !== 'oui'
		) {
			// -- Construction de la liste des valeurs possibles pour le choix de l'encapsulation
			$config_encapsulation = ncore_noisette_initialiser_encapsulation($plugin)
				? _T('ncore:saisie_encapsulation_oui_info')
				: _T('ncore:saisie_encapsulation_non_info');
			$options_encapsulation = [
				'defaut' => _T('ncore:saisie_encapsulation_defaut_option', ['defaut' => lcfirst($config_encapsulation)]),
				'oui'    => _T('ncore:saisie_encapsulation_oui_option'),
				'non'    => _T('ncore:saisie_encapsulation_non_option')
			];

			// -- Construction de la saisie de la méthode d'encapsulation
			$saisie_encapsulation[] = [
				'saisie'  => 'radio',
				'options' => [
					'nom'   => 'encapsulation',
					'label' => '<:ncore:saisie_encapsulation_label:>',
					'data'  => $options_encapsulation,
				]
			];
		} else {
			// On utilise pas l'indicateur d'encapsulation, on le fixe donc dans un hidden
			$saisies[] = [
				'saisie'  => 'hidden',
				'options' => [
					'nom' => 'encapsulation',
				]
			];
		}

		// -- On insère le groupe de saisies dans la variable finale
		$saisies_par_groupe['avance'] = array_merge($saisies_par_groupe_defaut['avance'], $saisie_encapsulation);

		// Insertion des groupes de saisies dans leur fieldset respectifs
		foreach ($saisies_par_groupe as $_groupe => $_saisies) {
			if ($saisies_par_groupe[$_groupe]) {
				$saisies[] = [
					'saisie'  => 'fieldset',
					'options' => [
						'nom'    => "fieldset_{$_groupe}",
						'label'  => "<:ncore:saisie_{$_groupe}_legende:>",
						'onglet' => 'oui'
					],
					'saisies' => $_saisies
				];
			}
		}
	}

	return $saisies;
}

/**
 * Chargement des données : la fonction charge les données d'initialisation des saisies définies dans la fonction
 * `formulaires_editer_noisette_saisies_dist()`.
 *
 * @uses noisette_lire()
 * @uses type_noisette_lire()
 *
 * @param string      $plugin      Identifiant qui permet de distinguer le module appelant. Pour un plugin, le plus pertinent
 *                                 est d'utiliser le préfixe.
 * @param int|string  $id_noisette Identifiant unique de la noisette.
 * @param null|string $redirect    URL de redirection en sortie du formulaire.
 * @param null|string $stockage    Identifiant du service de stockage à utiliser si précisé.
 *
 * @return array Tableau des variables initialisées pour les saisies.
 */
function formulaires_editer_noisette_charger_dist(string $plugin, $id_noisette, ?string $redirect = '', ?string $stockage = '') : array {
	$valeurs = ['editable' => false];

	if (
		include_spip('ncore/ncore')
		and ($id_noisette = ncore_noisette_verifier_identifiant($plugin, $id_noisette, $stockage))
		and autoriser('editer', 'noisette', $id_noisette, null, ['plugin' => $plugin, 'stockage' => $stockage])
	) {
		// Récupération des informations sur la noisette en cours d'édition et sur le type de noisette
		include_spip('inc/ncore_noisette');
		$noisette = noisette_lire(
			$plugin,
			$id_noisette,
			'',
			false,
			$stockage
		);

		if ($noisette) {
			// Id de la noisette
			$valeurs['id_noisette'] = $id_noisette;
			// Type de la noisette
			$valeurs['type_noisette'] = $noisette['type_noisette'];
			$valeurs['est_conteneur'] = $noisette['est_conteneur'];

			// Insérer dans le contexte les valeurs des paramètres spécifiques stockées en BD.
			// On doit passer par saisies_charger_champs() au cas ou la définition de la noisette a changé
			// et qu'il y a de nouveaux champs à prendre en compte
			include_spip('inc/saisies');
			include_spip('inc/ncore_type_noisette');
			$type_noisette = type_noisette_lire(
				$plugin,
				$noisette['type_noisette'],
				'',
				false,
				$stockage
			);
			$valeurs = array_merge(
				$valeurs,
				saisies_charger_champs($type_noisette['parametres']),
				$noisette['parametres']
			);

			// Insérer dans le contexte le paramètre d'encapsulation stocké en BD
			// (ce paramètre n'est utilisé que si le mode d'encapsulation automatique a été débrayé et que la noisette
			// n'est pas un conteneur).
			$valeurs['encapsulation'] = $noisette['encapsulation'];

			// Insérer les css applicables soit à la capsule, soit au conteneur en affectant correctement la valeur
			// compilée du champ css de la noisette à chacune des saisies css.
			$valeurs['css_saisies'] = $noisette['css_saisies'];

			$valeurs['editable'] = true;
		}
	} else {
		$valeurs['message_erreur'] = _T('ncore:erreur_noisette_edition_interdite');
	}

	return $valeurs;
}

/**
 * Exécution du formulaire : les modifications effectuées sur les paramètres de la noisette sont enregistrés
 * dans l'espace de stockage du plugin utilisateur.
 *
 * @uses type_noisette_lire()
 * @uses ncore_noisette_initialiser_encapsulation()
 * @uses noisette_parametrer()
 *
 * @param string      $plugin      Identifiant qui permet de distinguer le module appelant. Pour un plugin, le plus pertinent
 *                                 est d'utiliser le préfixe.
 * @param int|string  $id_noisette Identifiant unique de la noisette.
 * @param null|string $redirect    URL de redirection en sortie du formulaire.
 * @param null|string $stockage    Identifiant du service de stockage à utiliser si précisé.
 *
 * @return array Tableau des messages de fin de traitement.
 */
function formulaires_editer_noisette_traiter_dist(string $plugin, $id_noisette, ?string $redirect = '', ?string $stockage = '') : array {
	$retour = [];

	if (
		include_spip('ncore/ncore')
		and ($id_noisette = ncore_noisette_verifier_identifiant($plugin, $id_noisette, $stockage))
		and autoriser('editer', 'noisette', $id_noisette, null, ['plugin' => $plugin, 'stockage' => $stockage])
	) {
		// On constitue le tableau des valeurs des paramètres spécifiques de la noisette
		include_spip('inc/ncore_type_noisette');
		$champs = type_noisette_lire(
			$plugin,
			_request('type_noisette'),
			'parametres',
			false,
			$stockage
		);
		$parametres = [];
		if ($champs) {
			include_spip('inc/saisies_lister');
			foreach (saisies_lister_champs($champs, false) as $_champ) {
				$parametres[$_champ] = _request($_champ);
			}
		}
		$valeurs = ['parametres' => serialize($parametres)];

		// Récupération des classes CSS saisies sous forme tabulaire
		// (nécessaire à ce stade car remis à zéro si pas d'encapsulation)
		if (!$css_saisies = _request('css_saisies')) {
			$css_saisies = [];
		}

		// Encapsulation de la noisette: uniquement si le mode d'encapsulation automatique a été débrayé et
		// pour les noisettes non conteneur.
		include_spip('ncore_fonctions');
		if (
			!_NCORE_ENCAPSULATION_AUTO
			and _request('est_conteneur') !== 'oui'
		) {
			include_spip('inc/ncore_noisette');
			$encapsulation = _request('encapsulation');
			if (
				($encapsulation === 'non')
				or (
					($encapsulation === 'defaut')
					and !ncore_noisette_initialiser_encapsulation($plugin)
				)
			) {
				// on remet à zéro les css si la capsule englobante n'est pas active
				$css_saisies = [];
			}
			$valeurs['encapsulation'] = $encapsulation;
		}

		// Traitement des CSS de la capsule ou du conteneur
		// -- préparation pour enregistrement en base
		$valeurs['css_saisies'] = serialize($css_saisies);
		// -- calcul de la chaine compilée des classes CSS
		$valeurs['css'] = '';
		foreach ($css_saisies as $_css) {
			$valeurs['css'] .= (is_array($_css) ? implode(' ', $_css) : $_css) . ' ';
		}
		$valeurs['css'] = rtrim($valeurs['css'], ' ');

		// Fermeture de la modale
		$autoclose = "<script type='text/javascript'>if (window.jQuery) jQuery.modalboxclose();</script>";

		// Mise à jour de la noisette en base de données
		include_spip('inc/ncore_noisette');
		if (noisette_parametrer($plugin, $id_noisette, $valeurs, $stockage)) {
			// On invalide le cache
			include_spip('inc/invalideur');
			suivre_invalideur("id='noisette/{$id_noisette}'");
			$retour['message_ok'] = _T('info_modification_enregistree') . $autoclose;
			if ($redirect) {
				if (strncmp($redirect, 'javascript:', 11) === 0) {
					$retour['message_ok'] .= '<script type="text/javascript">/*<![CDATA[*/' . substr($redirect, 11) . '/*]]>*/</script>';
					$retour['editable'] = true;
				} else {
					$retour['redirect'] = $redirect;
				}
			}
		} else {
			$retour['message_erreur'] = _T('ncore:erreur_noisette_edition_nok');
		}
	} else {
		$retour['message_erreur'] = _T('ncore:erreur_noisette_edition_nok');
	}

	return $retour;
}
